import 'package:flutter/material.dart';

Map<String, Color> themeColorMap = {
  'blue': Colors.blue,
  'blueAccent': Colors.blueAccent,
  'cyan': Colors.cyan,
  'deepPurple': Colors.purple,
  'deepPurpleAccent': Colors.deepPurpleAccent,
  'deepOrange': Colors.orange,
  'green': Colors.green,
  'indigo': Colors.indigo,
  'indigoAccent': Colors.indigoAccent,
  'orange': Colors.orange,
  'purple': Colors.purple,
  'pink': Colors.pink,
  'red': Colors.red,
  'teal': Colors.teal,
  'black': Color(0xff1A1C37),
};


class AppInfoProvider with ChangeNotifier {
  String _themeColor = '';
  String get themeColor => _themeColor;
  setTheme(String themeColor) {
    var s = _themeColor = themeColor;
    print(s);
    notifyListeners();
  }
}
TextTheme textTheme(BuildContext context) {
  return Theme.of(context).primaryTextTheme;
}
