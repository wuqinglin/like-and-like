import 'package:flutter/material.dart';
import 'package:flutter_dz/Base/MainTabbarPage.dart';
import 'package:flutter_dz/MainPage/My/SetupColorsPage.dart';
//配置路由
final routes = {
  '/':(content) => MainTabbarPage(),
  '/SetupColorsPage': (content) => SetupColorsPage()
};
//固定写法
var onGenerateRoute = (RouteSettings settings) {
  final String name = settings.name;
  final Function pageContentBuilder = routes[name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      return MaterialPageRoute(
          builder: (content) =>
              pageContentBuilder(content, arguments: settings.arguments));
    } else {
      return MaterialPageRoute(
          builder: (content) => pageContentBuilder(content));
    }
  }
};
