import 'entity_factory.dart';

//返回状态类型
enum HTTPSTATE {
  SUCCESS,
  ERROR,
  LOGIN
}
/// Object基类
class BaseEntity<T> {
  int code;
  String message;
  T data;

  BaseEntity({this.code, this.message, this.data});

  factory BaseEntity.fromJson(json, {bool isBanquet = false,}) {
    return BaseEntity(
        code: json['code'] == null ? 0 : json['code'],
        message: json['msg'],
        data: EntityFactory.generateOBJ<T>(json['data'])
    );
  }
}

/// List<Object>基类
class BaseListEntity<T> {
  int code;
  String message;
  List<T> data;

  BaseListEntity({this.code, this.message, this.data});

  factory BaseListEntity.fromJson(json, {bool isBanquet = false,}) {
    List<T> mData = List();
    if (json['data'] != null) {
      (json['data'] as List).forEach((element)
      {
        mData.add(EntityFactory.generateOBJ<T>(element));
      });
    }

    return BaseListEntity(
        code: json[isBanquet ? 'returncode' : 'code'],
        message: json['message'],
        data: mData
    );
  }
}

/// 服务端返回错误类
class FailEntity {
  int code;
  String message;
  FailEntity({this.code, this.message});
}

/// http错误类
class ErrorEntity {
  int code;
  String message;
  ErrorEntity({this.code, this.message});
}

/// http CodeMessage类
class FlagEntity {
  int code;
  String message;
  FlagEntity({this.code, this.message});
}