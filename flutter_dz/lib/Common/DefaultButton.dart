import 'package:flutter/material.dart';

class DefaultTextButton extends StatefulWidget {
  final GestureTapCallback onPressed;
  final Widget nomalWidget;
  final Color backgroundColor;
  final Size buttonSize;
  final EdgeInsetsGeometry margin;
  final bool isRadius;
  const DefaultTextButton(
      {Key key,
      this.onPressed,
      this.nomalWidget,
      this.backgroundColor,
      this.buttonSize,
      this.margin,this.isRadius})
      : super(key: key);
  @override
  _DefaultTextButtonState createState() => _DefaultTextButtonState();
}

class _DefaultTextButtonState extends State<DefaultTextButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      width: widget.buttonSize.width,
      height: widget.buttonSize.height,
      child: FlatButton(
        color: widget.backgroundColor,
        highlightColor: widget.backgroundColor,
        child: widget.nomalWidget,
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(widget.isRadius ? widget.buttonSize.height/2:0)),
        onPressed: widget.onPressed,
      ),
    );
  }
}
