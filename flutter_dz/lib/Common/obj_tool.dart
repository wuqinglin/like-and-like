import 'package:flutter/cupertino.dart';
import 'package:flutter_dz/Base/MainTabbarPage.dart';

/// 检查对象或 List 或 Map 是否为空
bool isEmpty(Object object) {
  if (object == null) return true;
  if (object is String && object.isEmpty) {
    return true;
  } else if (object is List && object.isEmpty) {
    return true;
  } else if (object is Map && object.isEmpty) {
    return true;
  }
  return false;
}


final PageRouteBuilder _homeRoute = new PageRouteBuilder(
  pageBuilder: (BuildContext context, _, __) {
    return MainTabbarPage();
  },
);

void goHome(BuildContext context) {
  Navigator.pushAndRemoveUntil(context, _homeRoute, (Route<dynamic> r) => false);
}