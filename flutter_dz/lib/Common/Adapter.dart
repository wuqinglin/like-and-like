import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/screenutil.dart';

/// 直接通过`类名称`访问里面的方法，方法为 静态方法
class ScreenAdapterUtils {
  static init(context,Size designedSize){
    ScreenUtil.init(context, designSize: designedSize, allowFontScaling: true);  //flutter_screenuitl >= 1.2
  }
  static sp(double value){
    return ScreenUtil().setSp(value, allowFontScalingSelf: true);  /// 获取 计算后的字体
  }
  static height(double value){
    return  ScreenUtil().setHeight(value);  /// 获取 计算后的高度
  }
  static width(double value){
    return ScreenUtil().setWidth(value);    /// 获取 计算后的宽度
  }
  static screenHeight(){
    return  ScreenUtil().screenHeight;  /// 获取 计算后的屏幕高度
  }
  static screenWidth(){
    return  ScreenUtil().screenWidth;  /// 获取 计算后的屏幕高度
  }
}

dynamic KadpHeight(double height) {
  return ScreenAdapterUtils.height(height);
}

dynamic KadpWidth(double width) {
  return ScreenAdapterUtils.width(width);
}

dynamic KFont(double sp) {
  return ScreenAdapterUtils.sp(sp);
}