import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef _InputCallBack = void Function(String value);

class WQTextField extends StatefulWidget {
  final Widget leftWidget;
  final Widget rightWidget;
  final _InputCallBack onChange;
  final String text;
  final String hintText;
  final TextStyle textStyle;
  final TextStyle hintTextStyle;
  final bool enabled; //是否可编辑，默认true
  final double inputH; //输入框高度 默认40
  final Color nomalBgColor; //默认背景色
  final Color selectBgColor; //选择背景色
  final Border nomalBorder;
  final Border selectBorder;
  final BorderRadius nomalBorderRadius;
  final bool obscureText;
  final EdgeInsets inputPadding;
  final int inputMaxLenth;
  final TextInputType inputType;
  const WQTextField(
      {Key key,
      this.leftWidget,
      this.rightWidget,
      this.onChange,
      this.text,
      this.hintText,
      this.enabled,
      this.inputH,
      this.nomalBgColor,
      this.selectBgColor,
      this.textStyle,
      this.hintTextStyle,
      this.nomalBorder,
      this.selectBorder,
      this.nomalBorderRadius,
      this.obscureText,
      this.inputPadding,
      this.inputMaxLenth = 20, this.inputType})
      : super(key: key);
  @override
  _WQTextFieldState createState() => _WQTextFieldState();
}

class _WQTextFieldState extends State<WQTextField> {
  TextEditingController _textController = TextEditingController();
  FocusNode _focusNode = FocusNode();
  Color _bgColor;
  Border _inputBorder;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textController.text = widget.text;
    _bgColor =
        widget.nomalBgColor != null ? widget.nomalBgColor : Colors.transparent;
    _inputBorder = widget.nomalBorder != null ? widget.nomalBorder : Border();
    if (widget.selectBgColor != null || widget.selectBorder != null) {
      _focusNode.addListener(() {
        if (_focusNode.hasFocus) {
          //有焦点
          if (widget.selectBgColor != null) {
            print('焦点了 部位空');
            _bgColor = widget.selectBgColor;
          }
          if (widget.selectBorder != null) {
            _inputBorder = widget.selectBorder;
          }
        } else {
          //失去焦点
          _bgColor = widget.nomalBgColor != null
              ? widget.nomalBgColor
              : Colors.transparent;
          _inputBorder =
              widget.nomalBorder != null ? widget.nomalBorder : Border();
        }
        setState(() {});
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.inputPadding != null
          ? widget.inputPadding
          : EdgeInsets.only(left: 10,right: widget.rightWidget != null? 0:10),
      decoration: BoxDecoration(
        border: _inputBorder,
        borderRadius: widget.nomalBorderRadius,
        color: _bgColor,
      ),
      child: Row(
        children: [
          Visibility(
              visible: true,
              child:
                  widget.leftWidget == null ? Container() : widget.leftWidget),
          Expanded(
              child: ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: widget.inputH == null ? 40 : widget.inputH,
            ),
            child: TextField(
              focusNode: _focusNode,
              controller: _textController,
              style: widget.textStyle,
              keyboardType: widget.inputType,
              inputFormatters: [
                LengthLimitingTextInputFormatter(widget.inputMaxLenth)
              ],
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 2.0),
                hintText: widget.hintText == null ? "" : widget.hintText,
                hintStyle: widget.hintTextStyle != null
                    ? widget.hintTextStyle
                    : TextStyle(),
                // contentPadding: EdgeInsets.all(10),
                border: OutlineInputBorder(borderSide: BorderSide.none),
              ),
              obscureText: widget.obscureText,
              onChanged: (str) {
                if (widget.onChange != null) {
                  widget.onChange(str);
                }
              },
            ),
          )),
          Visibility(
              visible: true,
              child: widget.rightWidget == null
                  ? Container()
                  : widget.rightWidget),
        ],
      ),
    );
  }
}
