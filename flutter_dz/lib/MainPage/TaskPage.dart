import 'package:flutter/material.dart';
import 'package:flutter_dz/Common/Adapter.dart';
import 'package:flutter_dz/Common/AppInfoProvider.dart';

class TaskPage extends StatefulWidget {
  @override
  _TaskPageState createState() => _TaskPageState();
}

class _TaskPageState extends State<TaskPage> {
  final List<Tab> myTabs = <Tab>[
    Tab(
      child: Text('进行中'),
    ),
    Tab(
      child: Text('已完成'),
    ),
    Tab(
      child: Text('失败'),
    )
  ];
  Text getTypeText(int type){
    //type : 1,正在审核(橘色) 2,已完成(绿色) 3,失败(红色) 4,任务进行中(白色)
    String title = '正在审核';
    Color titleColor = Color(0xffFF6C01);
    if(type == 1) {
      title = '正在审核';
      titleColor = Color(0xffFF6C01);
    } else if (type == 2) {
      title = '已完成';
      titleColor = Colors.green;
    } else if (type == 3) {
      title = '失败';
      titleColor = Colors.red;
    } else {
      title = '任务进行中';
      titleColor = Colors.white;
    }
    return Text(title,style: TextStyle(
        fontSize: KFont(32),
        color: titleColor
    ));
  }
  Text getActionText(int type){
    //type : 1,正在审核(橘色) 2,已完成(绿色) 3,失败(红色) 4,任务进行中(白色)
    String title = '放弃';
    Color titleColor = Color(0xffFF6C01);
    if(type == 1) {
      title = '放弃';
      titleColor = Color(0xffBCBDC5);
    } else if (type == 2) {
      title = '';
      titleColor = Colors.green;
    } else if (type == 3) {
      title = '申诉';
      titleColor = Colors.red;
    } else {
      title = '提交';
      titleColor = Colors.white;
    }
    return Text(title,style: TextStyle(
        fontSize: KFont(32),
        color: titleColor
    ));
  }
  Widget getRwCell({Map dic}){


    return Container(
      padding: EdgeInsets.only(left: KadpWidth(30),right: KadpWidth(30)),
      margin: EdgeInsets.only(top: KadpWidth(40)),
      color: Color(0xff21233D),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: KadpWidth(18),bottom: KadpWidth(18)),
            child: Row(
              children: [
                getTypeText(1),
                Spacer(),
                Text('单价10元',style: TextStyle(
                  fontSize: KFont(28),
                  color: Colors.white
                ))
              ],
            ),
          ),
          Row(
            children: [
              Container(
                width: KadpWidth(100),
                height: KadpWidth(100),
                child: CircleAvatar(
                  backgroundImage: NetworkImage('https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=58179169,2410235469&fm=111&gp=0.jpg'),
                ),
              ),
              SizedBox(width: KadpWidth(32)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Facebook任务需求',style: TextStyle(
                    fontSize: KFont(32),
                    color: Colors.white
                  )),
                  Text('开始时间: 2020.12.12-7:36',style: TextStyle(
                      fontSize: KFont(24),
                      color: Color(0xffBCBDC5)
                  )),
                  Text('结束时间: 2020.12.12-7:36',style: TextStyle(
                      fontSize: KFont(24),
                      color: Color(0xffBCBDC5)
                  ))
                ],
              ),
              Spacer(),
              Icon(Icons.pages,size: KadpWidth(98),color: Colors.grey)
            ],
          ),
          Padding(padding: EdgeInsets.only(top: KadpWidth(32),bottom: KadpWidth(32)),child: Row(
            children: [
              Text('打开网站',style: TextStyle(
                color: textTheme(context).subtitle1.color,
                fontSize: KFont(24)
              )),
              SizedBox(width: KadpWidth(26)),
              Text('复制链接',style: TextStyle(
                  color: textTheme(context).subtitle1.color,
                  fontSize: KFont(24)
              )),
              Spacer(),
              getActionText(1)
            ],
          ))
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
              title: TabBar(
                  indicatorColor: Colors.transparent,
                  labelColor: Colors.blue,
                  unselectedLabelColor: Color(0xff6D6F7D),
                  tabs: myTabs,onTap: (index){
                    print('点击了第${index}个');
              })),
          body: TabBarView(children: [
            Container(
              child: ListView(
                children: [
                  getRwCell(),
                  getRwCell(),
                  getRwCell()
                ],
              ),
            ),
            Container(
              child: Center(
                child: Text('已完成'),
              ),
            ),
            Container(
              child: Center(
                child: Text('失败'),
              ),
            )
          ])),
    );
  }
}
