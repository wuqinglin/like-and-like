import 'package:flutter/material.dart';
import 'package:flutter_dz/Common/AppInfoProvider.dart';
import 'package:flutter_dz/Common/Adapter.dart';

class UserHeadView extends StatefulWidget {
  @override
  _UserHeadViewState createState() => _UserHeadViewState();
}

class _UserHeadViewState extends State<UserHeadView> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Container(
                width: KadpWidth(120),
                height: KadpWidth(120),
                child: CircleAvatar(
                  // backgroundImage: NetworkImage(
                  //     (kDebugMode?Address.debugBaseUrl : Address.releaseBaseUrl)+dic['avatar']),
                  backgroundImage: NetworkImage('https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=58179169,2410235469&fm=111&gp=0.jpg')
                ),
              ),
              SizedBox(width: KadpWidth(26)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('昵称：453645',style: TextStyle(color: Colors.white,fontSize: KFont(24))),
                  SizedBox(height: KadpWidth(6)),
                  Text('邀请码：453645',style: TextStyle(color: Colors.white,fontSize: KFont(24))),
                  SizedBox(height: KadpWidth(6)),
                  Text('昵称：453645',style: TextStyle(color: Colors.white,fontSize: KFont(24))),
                ],
              ),
              Spacer(),
              Column(
                children: [
                  Icon(Icons.card_giftcard,color: Colors.grey,size: KadpWidth(53)),
                  Text('88币',style: TextStyle(
                    color: Colors.white,
                    fontSize: KadpWidth(24)
                  ))
                ],
              )
            ],
          ),
          SizedBox(height: KadpWidth(40)),
          Container(
            height: KadpWidth(60),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  child: Row(
                    children: [
                      Text('立即完成任务',style: TextStyle(
                        color:Color(0xff6B6D7B),
                        fontSize: KFont(28)
                      )),
                      SizedBox(width: KadpWidth(20)),
                      Text('0',style: TextStyle(
                        color: Colors.white,
                        fontSize: KFont(40)
                      ))
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Text('今日剩余任务',style: TextStyle(
                          color:Color(0xff6B6D7B),
                          fontSize: KFont(28)
                      )),
                      SizedBox(width: KadpWidth(20)),
                      Text('0',style: TextStyle(
                          color: Colors.white,
                          fontSize: KFont(40)
                      ))
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: KadpWidth(40)),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              color: Color(0xff21233D)
            ),
            padding: EdgeInsets.only(top: KadpWidth(40),bottom: KadpWidth(40)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  child: Column(
                    children: [
                      Text('88',style: TextStyle(
                        color: Colors.white,
                        fontSize: KFont(40)
                      )),
                      Text('今日收益',style: TextStyle(
                          color: textTheme(context).subtitle1.color,
                          fontSize: KFont(24)
                      ))
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Text('88',style: TextStyle(
                          color: Colors.white,
                          fontSize: KFont(40)
                      )),
                      Text('本周收益',style: TextStyle(
                          color: textTheme(context).subtitle1.color,
                          fontSize: KFont(24)
                      ))
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Text('88',style: TextStyle(
                          color: Colors.white,
                          fontSize: KFont(40)
                      )),
                      Text('本月收益',style: TextStyle(
                          color: textTheme(context).subtitle1.color,
                          fontSize: KFont(24)
                      ))
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Text('88',style: TextStyle(
                          color: Colors.white,
                          fontSize: KFont(40)
                      )),
                      Text('总收益',style: TextStyle(
                          color: textTheme(context).subtitle1.color,
                          fontSize: KFont(24)
                      ))
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}


class MyPage extends StatelessWidget {
  //底部行Cell
  Widget getRowCell(String imgName,String title,{bool isLine}){
    return Container(
      height: KadpWidth(114),
      decoration: (isLine != null && isLine == true)? BoxDecoration(
          border: Border(bottom: BorderSide(width: 1,color: Color(0xff4A576A)))
      ):null,
      child: Row(
        children: [
          Icon(Icons.error,size: KadpWidth(37),color: Colors.white),
          SizedBox(width: KadpWidth(10)),
          Text(title,style: TextStyle(
            fontSize: KFont(28),
            color: Colors.white
          )),
          Spacer(),
          Icon(Icons.chevron_right,size: KadpWidth(47),color: Color(0xff4A576A))
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(elevation: 0),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: KadpWidth(30),right: KadpWidth(30)),
            child: Column(
              children: [
                UserHeadView(),
                Container(
                  padding: EdgeInsets.only(top: KadpWidth(88),bottom: KadpWidth(56)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        child: Column(
                          children: [
                            Image.asset('Images/home_jg_icon.png',width: KadpWidth(74),height: KadpWidth(76)),
                            SizedBox(height: KadpWidth(18)),
                            Text('任务记录',style: TextStyle(color: Colors.white,fontSize: KFont(28)))
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Image.asset('Images/home_jg_icon.png',width: KadpWidth(74),height: KadpWidth(76)),
                            SizedBox(height: KadpWidth(18)),
                            Text('发布管理',style: TextStyle(color: Colors.white,fontSize: KFont(28)))
                          ]
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Image.asset('Images/home_jg_icon.png',width: KadpWidth(74),height: KadpWidth(76)),
                            SizedBox(height: KadpWidth(18)),
                            Text('审核任务',style: TextStyle(color: Colors.white,fontSize: KFont(28)))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: KadpWidth(74)),
                  padding: EdgeInsets.only(left: KadpWidth(56),right: KadpWidth(56)),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(KadpWidth(5)),
                    color: Color(0xff21233D)
                  ),
                  child: Column(
                    children: [
                      getRowCell('','个人信息',isLine:true),
                      getRowCell('','分享好友',isLine:true),
                      getRowCell('','绑定账号',isLine:true),
                      getRowCell('','团队统计',isLine:false)
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
