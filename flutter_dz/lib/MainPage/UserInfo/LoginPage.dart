import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dz/Base/MainTabbarPage.dart';
import 'package:flutter_dz/Common/Adapter.dart';
import 'package:flutter_dz/Common/AppInfoProvider.dart';
import 'package:flutter_dz/Common/Http/Http.dart';
import 'package:flutter_dz/Common/obj_tool.dart';
import 'package:flutter_dz/MainPage/HomePage.dart';
import 'package:nav_router/nav_router.dart';

import 'RegistPage.dart';

//手机号
String _phoneNumber = '';
//密码
String _password = '';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  //登录接口
  loginNet() async {
    if (isEmpty(_phoneNumber) && _phoneNumber.length == 0) {
      BotToast.showText(text: '请输入正确的手机号');
      return;
    }
    if (isEmpty(_password) && _password.length == 0) {
      BotToast.showText(text: '密码不可为空');
      return;
    }
    BotToast.showLoading();
    Http.instance
        .post('/login', {'phonenumber': _phoneNumber, 'password': _password},
            onSuccess: (data) {
      //回到首页
      goHome(context);
      print('打印登录接口返回：$data');
    }, onFlagCallBack: (msg) {
      BotToast.showText(text: msg.message);
    }, complete: () {
      BotToast.closeAllLoading();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(elevation: 0),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Color(0xff68B6FF),
              width: KadpWidth(160),
              height: KadpWidth(160),
            ),
            SizedBox(
              height: KadpWidth(100),
            ),
            Container(
              padding: EdgeInsets.all(KadpWidth(90)),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text('登录',
                          style: TextStyle(
                              fontSize: KFont(48),
                              color: textTheme(context).bodyText1.color)),
                      Spacer(),
                      GestureDetector(
                        child: Text('没有账号,点击登录>',
                            style: TextStyle(
                                color: textTheme(context).subtitle1.color,
                                fontSize: KFont(24))),
                        onTap: () {
                          routePush(RegistPage()).then((value) {
                            _phoneNumber = value;
                            setState(() {});
                          });
                        },
                      )
                    ],
                  ),
                  SizedBox(
                    height: KadpWidth(50),
                  ),
                  HeadTextF(
                    '请输入手机号',
                    text: _phoneNumber,
                    inputMaxLenth: 11,
                    ontap: (str) {
                      _phoneNumber = str;
                    },
                  ),
                  HeadTextF(
                    '请输入密码',
                    obscureText: true,
                    ontap: (str) {
                      _password = str;
                    },
                  ),
                  SizedBox(height: KadpWidth(109)),
                  Container(
                    width: KadpWidth(581),
                    height: KadpWidth(91),
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(5)),
                    child: FlatButton(
                      onPressed: loginNet,
                      child: Text(
                        '登录',
                        style:
                            TextStyle(color: Colors.white, fontSize: KFont(34)),
                      ),
                      shape: RoundedRectangleBorder(
                          side: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(50))),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
