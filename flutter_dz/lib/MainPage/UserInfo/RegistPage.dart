import 'dart:convert';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dz/Common/Adapter.dart';
import 'package:flutter_dz/Common/AppInfoProvider.dart';
import 'package:flutter_dz/Common/Http/Http.dart';
import 'package:flutter_dz/Common/jh_count_down_btn.dart';
import 'package:flutter_dz/Common/obj_tool.dart';
import 'package:flutter_dz/Common/wq_textfield.dart';
import 'package:nav_router/nav_router.dart';

typedef OnTap = Function(String title);
typedef OnCode = Function();
//手机号
String _phoneNumber;
//验证码
String _code;
//邀请码
String _yqCode;
//密码
String _password;
//再次输入
String _newPassword;

class HeadTextF extends StatelessWidget {
  final String text;
  final String headTitle;
  final String placeText;
  final type; //0位普通 1为验证码
  final OnTap ontap;
  final OnCode onCode;
  final bool obscureText;
  final int inputMaxLenth;
  final TextInputType inputType;
  const HeadTextF(this.headTitle,
      {Key key,
      this.placeText,
      this.type,
      this.ontap,
      this.obscureText = false,
      this.inputMaxLenth,
      this.inputType,
      this.onCode, this.text})
      : super(key: key);

  ///0普通输入框 1验证码

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: KadpWidth(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(this.headTitle,
              style: TextStyle(
                  color: textTheme(context).subtitle1.color,
                  fontSize: KFont(24))),
          SizedBox(height: KadpWidth(16)),
          WQTextField(
            inputH: KadpWidth(90),
            nomalBorder:
                Border.all(width: 1, color: textTheme(context).subtitle1.color),
            nomalBorderRadius: BorderRadius.circular(3),
            obscureText: obscureText,
            inputMaxLenth: inputMaxLenth,
            textStyle: TextStyle(color: Colors.white),
            inputType: inputType,
            text: text,
            rightWidget: type == 1
                ? JhCountDownBtn(
                    getVCode: () async {
                      print('_phoneNumber : $_phoneNumber');
                      if (isEmpty(_phoneNumber) || _phoneNumber.length == 0) {
                        BotToast.showText(text: '请输入正确的手机号');
                        return false;
                      }
                      await onCode();
                      return true;
                    },
                    textColor: Colors.blue,
                  )
                : null,
            onChange: (str) {
              ontap(str);
            },
            selectBgColor: textTheme(context).subtitle2.color,
          )
        ],
      ),
    );
  }
}

class RegistPage extends StatelessWidget {
  //注册接口
  registNet() async {
    if (isEmpty(_phoneNumber) && _phoneNumber.length == 0) {
      BotToast.showText(text: '请填写正确的手机号');
      return;
    }
    if (isEmpty(_code) && _code.length == 0) {
      BotToast.showText(text: '验证码不能为空');
      return;
    }
    if (isEmpty(_yqCode) && _yqCode.length == 0) {
      BotToast.showText(text: '请填写正确的邀请码');
      return;
    }
    if (isEmpty(_password) && _password.length == 0) {
      BotToast.showText(text: '密码不能为空');
      return;
    }
    if (isEmpty(_newPassword) && _newPassword == _password) {
      BotToast.showText(text: '两次输入的密码不一致');
      return;
    }
    Map<String, String> netDic = {
      'phonenumber':_phoneNumber,
      'mcode':_code,
      'pInviCode':_yqCode,
      'password':_newPassword
    };
    BotToast.showLoading();
    Http.instance.post('/register', netDic, onSuccess: (data) {
      pop(_phoneNumber);
    },onFlagCallBack: (msg){
      BotToast.showText(text: msg.message);
    },complete: (){
      BotToast.closeAllLoading();
    });
  }

  //获取验证码接口
  getCodeNet() async {
    BotToast.showLoading();
    Http.instance.get('/getMcode', {'phonenumber': _phoneNumber},
        onSuccess: (data) {
          BotToast.showText(text: '验证码已发送');
        },
        onFlagCallBack: (msg){
          BotToast.showText(text: msg.message);
        },
        complete: () {
          BotToast.closeAllLoading();
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('注册'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(KadpWidth(86)),
          child: Column(
            children: [
              HeadTextF('请输入手机号',
                  inputMaxLenth: 11,
                  inputType: TextInputType.number, ontap: (str) {
                _phoneNumber = str;
              }),
              HeadTextF('请输入验证码', inputType: TextInputType.number, type: 1,
                  ontap: (str) {
                _code = str;
              }, onCode: getCodeNet),
              HeadTextF('邀请人', ontap: (str) {
                _yqCode = str;
              }),
              HeadTextF('密码', obscureText: true, ontap: (str) {
                _password = str;
              }),
              HeadTextF('确认密码', obscureText: true, ontap: (str) {
                _newPassword = str;
              }),
              SizedBox(height: KadpWidth(62)),
              Container(
                width: KadpWidth(581),
                height: KadpWidth(91),
                decoration: BoxDecoration(
                    color: Colors.blue, borderRadius: BorderRadius.circular(5)),
                child: FlatButton(
                  onPressed: registNet,
                  child: Text(
                    '注册',
                    style: TextStyle(color: Colors.white, fontSize: KFont(34)),
                  ),
                  shape: RoundedRectangleBorder(
                      side: BorderSide.none,
                      borderRadius: BorderRadius.all(Radius.circular(50))),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
