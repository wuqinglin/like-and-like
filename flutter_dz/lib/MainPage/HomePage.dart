import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dz/Common/Adapter.dart';
import 'package:flutter_dz/Common/AppInfoProvider.dart';
import 'package:flutter_dz/Common/Http/Address.dart';
import 'package:flutter_dz/Common/Http/Http.dart';
import 'package:flutter_dz/generated/l10n.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import 'Home/FontMarquee.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  //轮播图数组
  List bannerList = [];
  //文字轮播数组
  List noticeList = [];
  //任务列表数组
  List taskList = [];

  //四个按钮
  Widget getMenuIcon(String imgName, String title) {
    return Container(
      child: Column(
        children: [
          Image.asset(imgName, width: KadpWidth(98), height: KadpWidth(98)),
          SizedBox(height: KadpWidth(22)),
          Text(title,
              style: TextStyle(
                  color: textTheme(context).subtitle1.color,
                  fontSize: KFont(24)))
        ],
      ),
    );
  }

  //专区
  Widget getzqIcon(String imgName, String title) {
    return Container(
      margin: EdgeInsets.only(top: KadpWidth(20)),
      color: Color(0xff21233D),
      height: KadpWidth(104),
      width: MediaQuery.of(context).size.width / 2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(imgName, width: KadpWidth(32), height: KadpWidth(32)),
          SizedBox(width: KadpWidth(5)),
          Text(title,
              style: TextStyle(
                  color: textTheme(context).subtitle1.color,
                  fontSize: KFont(30)))
        ],
      ),
    );
  }

  //任务
  Widget getrwIcon(String imgName, String title, String subTitle) {
    return Container(
      margin: EdgeInsets.only(top: KadpWidth(20)),
      color: Color(0xff21233D),
      height: KadpWidth(192),
      width: MediaQuery.of(context).size.width / 2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(imgName, width: KadpWidth(84), height: KadpWidth(84)),
          SizedBox(width: KadpWidth(50)),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(title,
                  style: TextStyle(color: Colors.white, fontSize: KFont(24))),
              SizedBox(height: KadpWidth(10)),
              Text(subTitle,
                  style: TextStyle(color: Colors.white, fontSize: KFont(26)))
            ],
          ),
        ],
      ),
    );
  }

  //任务cell
  Widget getrwCell(Map dic) {
    return Container(
      color: Color(0xff21233D),
      margin: EdgeInsets.only(top: KadpWidth(20)),
      padding: EdgeInsets.only(
          left: KadpWidth(30),
          right: KadpWidth(30),
          top: KadpWidth(24),
          bottom: KadpWidth(24)),
      child: Row(
        children: [
          CircleAvatar(
            backgroundImage: NetworkImage(
                (kDebugMode?Address.debugBaseUrl : Address.releaseBaseUrl)+dic['avatar']),
          ),
          SizedBox(width: KadpWidth(36)),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('恭喜${dic['nickName'].toString()}',
                  style: TextStyle(color: Colors.white, fontSize: KFont(28))),
              SizedBox(height: KadpWidth(12)),
              Text('今日完成${dic['taskNum'].toString()}任务',
                  style: TextStyle(
                      color: textTheme(context).subtitle2.color,
                      fontSize: KFont(26)))
            ],
          ),
          Spacer(),
          Container(
            height: KadpWidth(50),
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(KadpWidth(25))
            ),
            child: FlatButton(
              minWidth: KadpWidth(114),
              child: Text(
                '${dic['coinNum'].toString()}币',
                style: TextStyle(color: Colors.white, fontSize: KFont(22)),
              ),
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> showTaskListCell(){
    // Future((){
    //   List<Widget> newArr = List();
    //   for(var i=0;i<taskList.length;i++){
    //     newArr.add(getrwCell(taskList[i]));
    //   }
    //   return newArr;
    // }).then((value) {
    //   return value;
    // });
    List<Widget> newArr = List();
    for(var i=0;i<taskList.length;i++){
      newArr.add(getrwCell(taskList[i]));
    }
    return newArr;
  }

  //首页接口
  getHomeNet(){
    Http.instance.get('/like/index', null,onSuccess: (Map data){
      // print('打印首页数据：$data');
      noticeList = data['noticeList'];
      bannerList = data['bannerList'];
      taskList = data['taskList'];
      print('noticeList:$noticeList \n bannerList:$bannerList \n taskList:$taskList');
      setState(() {

      });
    },onFlagCallBack: (msg){

    },complete: (){

    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getHomeNet();
    print('进来了');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          FlatButton(
              onPressed: () {
                print('点击了客服');
              },
              child: Text(S.of(context).service,
                  style: TextStyle(color: textTheme(context).subtitle1.color)))
        ],
      ),
      body: Container(
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Column(
            children: [
              bannerList.length > 0? Container(
                height: KadpWidth(286),
                padding: EdgeInsets.all(KadpWidth(30)),
                child: Swiper(
                  autoplay: true,
                  itemBuilder: (BuildContext context, int index) {
                    Map bannerDic = bannerList[index];
                    return Image.network(
                      (kDebugMode?Address.debugBaseUrl : Address.releaseBaseUrl)+bannerDic['bannerImg'],
                      fit: BoxFit.cover,
                    );
                  },
                  itemCount: bannerList.length,
                  pagination: SwiperPagination(),
                  control: SwiperControl(),
                ),
              ):Container(
                height: KadpWidth(286),
                padding: EdgeInsets.all(KadpWidth(30)),

              ),
              Container(
                color: Color(0xff21233D),
                padding: EdgeInsets.only(left: KadpWidth(41),right: KadpWidth(41)),
                height: KadpWidth(41),
                child: Row(
                  children: [
                    Icon(Icons.info,size: KadpWidth(28),color: Colors.white),
                    SizedBox(width: KadpWidth(44)),
                    Expanded(
                      child: FontMarquee(
                          noticeList:noticeList
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.only(top: KadpWidth(42), bottom: KadpWidth(42)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                        child: getMenuIcon('Images/home_jg_icon.png', '合作机构'),
                        onTap: () {
                          print('合作机构');
                        }),
                    GestureDetector(
                        child: getMenuIcon('Images/home_fw_icon.png', '会员服务'),
                        onTap: () {
                          print('会员服务');
                        }),
                    GestureDetector(
                        child: getMenuIcon('Images/home_jc_icon.png', '视频教程'),
                        onTap: () {
                          print('视频教程');
                        }),
                    GestureDetector(
                        child: getMenuIcon('Images/home_cx_icon.png', '优惠促销'),
                        onTap: () {
                          print('优惠促销');
                        }),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                      child: getzqIcon('Images/home_zs.png', '会员专区'),
                      onTap: () {
                        print('会员专区');
                      }),
                  GestureDetector(
                      child: getzqIcon('Images/home_dd.png', '商家专区'),
                      onTap: () {
                        print('商家专区');
                      })
                ],
              ),
              Row(
                children: [
                  GestureDetector(
                      child: getrwIcon(
                          'Images/home_cx_icon.png', 'YouTube', '领取任务'),
                      onTap: () {
                        print('点击了第一个任务');
                      }),
                  GestureDetector(
                      child: getrwIcon(
                          'Images/home_cx_icon.png', 'YouTube', '领取任务'),
                      onTap: () {
                        print('点击了第二个任务');
                      })
                ],
              ),
              Column(
                children: showTaskListCell(),
              ),


            ],
          ),
        ),
      ),
    );
  }
}
