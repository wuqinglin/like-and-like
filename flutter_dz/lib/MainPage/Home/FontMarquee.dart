import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_dz/Common/Adapter.dart';
import 'package:flutter_dz/Common/AppInfoProvider.dart';

//文字滚动效果
class FontMarquee extends StatefulWidget {
  final List noticeList;
  const FontMarquee({Key key, this.noticeList}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return FontMarqueeState();
  }
}

class FontMarqueeState extends State<FontMarquee> with WidgetsBindingObserver {
  GlobalKey _myKey = new GlobalKey();
  ScrollController _controller;
  int index = 0;
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    //来监听 节点是否build完成
    WidgetsBinding widgetsBinding = WidgetsBinding.instance;
    widgetsBinding.addPostFrameCallback((callback) {
      Timer.periodic(new Duration(seconds: 1), (timer) {
        index += _myKey.currentContext.size.height.toInt();

        _controller.animateTo((index).toDouble(),
            duration: new Duration(seconds: 1), curve: Curves.easeOutSine);
        //滚动到底部从头开始
        if ((index - _myKey.currentContext.size.height.toInt()).toDouble() >
            _controller.position.maxScrollExtent) {
          _controller.jumpTo(_controller.position.minScrollExtent);
          index = 0;
        }
      });
    });
    _controller = new ScrollController(initialScrollOffset: 0);
    /*   _controller.addListener(() {
      print(_controller.offset);
    });*/
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.noticeList.length > 0
        ? ListView.builder(
            key: _myKey,
            //禁止手动滑动
            physics: new NeverScrollableScrollPhysics(),
            itemCount: widget.noticeList.length,
            //item固定高度
            itemExtent: KadpWidth(41),
            scrollDirection: Axis.vertical,
            controller: _controller,
            itemBuilder: (context, index) {
              Map notiDic = widget.noticeList[index];
              return Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  notiDic['noticeTitle'].toString(),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: KFont(24),
                      fontWeight: FontWeight.bold,
                      color: textTheme(context).subtitle1.color),
                ),
              );
            })
        : Container();
  }
}
