import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dz/Base/MainTabbarPage.dart';
import 'package:flutter_dz/Common/AppInfoProvider.dart';
import 'package:flutter_dz/generated/l10n.dart';
import 'package:nav_router/nav_router.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'SpHelper.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Color _themeColor = Color(0xff1A1C37);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initThemeCoolor();
  }

  void _initThemeCoolor() async {
    await SpHelper.getInstance();
    String colorKey = SpHelper.getString('key_theme_color');
    _themeColor = themeColorMap[colorKey];
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: AppInfoProvider())],
      child: Consumer<AppInfoProvider>(builder: (context, appInfo, _) {
        String colorKey = appInfo.themeColor;
        print('收到通知:${colorKey}');
        if (themeColorMap[colorKey] != null) {
          _themeColor = themeColorMap[colorKey];
        }
        return MaterialApp(
            builder: BotToastInit(),
            localizationsDelegates: [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
            debugShowCheckedModeBanner: false,
            supportedLocales: S.delegate.supportedLocales,
            localeListResolutionCallback: (locales, supportedLocales) {
              print(locales);
              return;
            },
            navigatorKey: navGK,
            // onGenerateRoute: Application.router.generator
            theme: ThemeData(
                primaryColor: _themeColor,
                accentColor: _themeColor,
                bottomAppBarColor: _themeColor,
                buttonColor: Colors.red,
                primaryTextTheme: TextTheme(
                    bodyText1: TextStyle(color: Colors.blue),
                    subtitle1: TextStyle(color: Color(0xff919191)),
                    subtitle2: TextStyle(color: Color(0xff42465D))),
                //选项卡中选定的选项卡指示器的颜色
                indicatorColor: Colors.white,
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                //主页面背景色
                scaffoldBackgroundColor: _themeColor,
                inputDecorationTheme: InputDecorationTheme()),
            home: MainTabbarPage(
              styleColor: _themeColor,
            ));
      }),
    );
  }
}
