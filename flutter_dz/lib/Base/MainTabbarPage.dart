import 'package:flutter/material.dart';
import 'package:flutter_dz/Common/Adapter.dart';
import 'package:flutter_dz/Common/AppInfoProvider.dart';
import 'package:flutter_dz/MainPage/HomePage.dart';
import 'package:flutter_dz/MainPage/TaskPage.dart';
import 'package:flutter_dz/MainPage/VipPage.dart';
import 'package:flutter_dz/MainPage/MyPage.dart';

import 'SpHelper.dart';

class MainTabbarPage extends StatefulWidget {
  final Color styleColor;

  const MainTabbarPage({Key key, this.styleColor}) : super(key: key);
  @override
  _MainTabbarPageState createState() => _MainTabbarPageState(styleColor);
}

class _MainTabbarPageState extends State<MainTabbarPage>
    with SingleTickerProviderStateMixin {
  Color _backColor;
  _MainTabbarPageState(Color styleColor) {
    _backColor = styleColor;
  }
  //页面
  final pages = <Widget>[
    HomePage(),
    TaskPage(),
    VipPage(),
    MyPage(),
  ];
  //title和图标
  List<String> titles = ['首页', '任务', '会员', '我的'];
  List<IconData> icons = [
    Icons.home,
    Icons.search,
    Icons.camera_alt,
    Icons.contacts
  ];
  //默认选择第几个
  int _currentIndex = 0;
  //初始化页面控制器，并设置默认选择页面
  final _controller = PageController(initialPage: 0);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    String colorKey = SpHelper.getString('key_theme_color');
    _backColor = themeColorMap[colorKey];
    print('进到tabbar中来了');
  }

  @override
  Widget build(BuildContext context) {
    ScreenAdapterUtils.init(context, Size(750, 1334));
    return Scaffold(
      // body: PageView(
      //   controller: _controller,
      //   children: pages,
      //   physics: NeverScrollableScrollPhysics(),
      // ),
      body: IndexedStack(
        index: _currentIndex,
        children: pages,
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.fixed,
          selectedFontSize: ScreenAdapterUtils.width(28),
          unselectedFontSize: ScreenAdapterUtils.width(28),
          backgroundColor: _backColor == null ? widget.styleColor : _backColor,
          selectedItemColor: Colors.blue,
          unselectedItemColor: Color(0xffAAAAAA),
          onTap: (int index) {
            if (index != _currentIndex) {
              setState(() {
                _currentIndex = index;
              });

            }
          },
          items: getBarItems()),
    );
  }

  List<BottomNavigationBarItem> getBarItems() {
    final _defaultColor = Colors.grey;
    final _activieColor = Colors.blue;

    List<BottomNavigationBarItem> list = [];
    for (int i = 0; i < titles.length; i++) {
      BottomNavigationBarItem barItem = BottomNavigationBarItem(
        icon: Icon(
          icons[i],
          color: _defaultColor,
        ),
        activeIcon: Icon(
          icons[i],
          color: _activieColor,
        ),
        // ignore: deprecated_member_use
        title: Text(
          titles[i],
          style: TextStyle(
              color: _currentIndex != i ? _defaultColor : _activieColor,
              fontSize: KFont(20)),
        ),
      );

      list.add(barItem);
    }
    return list;
  }
}
